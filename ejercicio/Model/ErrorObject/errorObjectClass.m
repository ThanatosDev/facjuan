//
//  errorObjectClass.m
//  ejercicio
//
//  Created by Mac-01 on 23/11/18.
//  Copyright © 2018 JuanSoto. All rights reserved.
//

#import "errorObjectClass.h"

@implementation errorObjectClass

-(instancetype)initWithURLDataTask:(NSURLSessionDataTask*)task{
    self = [super init];
    
    if (self){
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
        NSLog(@"status code: %@",task.response);

        NSLog(@"status code: %ld",(long)httpResponse.statusCode);
        
        self.message=@"";
        self.title=@"";
        self.statusCode = [NSString stringWithFormat:@"%ld",(long)httpResponse.statusCode];
        self.requestURL= [NSString stringWithFormat:@"%@",httpResponse.URL];
        
        switch (httpResponse.statusCode) {
            case 400:
                self.message=@"La solicitud contiene sintaxis errónea y no debería repetirse.";
                self.title=@"Bad request";
                break;
            case 401:
                self.message=@"Vea autenticación HTTP básica y Digest access authentication.";
                self.title=@"Unauthorized";
                break;
            case 402:
                self.message=@"La intención original era que este código pudiese ser usado como parte de alguna forma o esquema de Dinero electrónico o micropagos, pero eso no sucedió, y este código nunca se utilizó.";
                self.title=@"Payment Required";
                break;
            case 403:
                self.message=@"La solicitud fue legal, pero el servidor rehúsa responderla dado que el cliente no tiene los privilegios para hacerla. En contraste a una respuesta 401 No autorizado, la autenticación no haría la diferencia.";
                self.title=@"Forbidden";
                break;
            case 404:
                self.message=@"Recurso no encontrado. Se utiliza cuando el servidor web no encuentra la página o recurso solicitado.";
                self.title=@"Not Found";
                break;
            case 422:
                self.message=@"Email ya utilizado";
                self.title=@"Email";
                break;
                
            default:
                break;
        }
    }
    return self;
}




@end
