//
//  errorObjectClass.h
//  ejercicio
//
//  Created by Mac-01 on 23/11/18.
//  Copyright © 2018 JuanSoto. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface errorObjectClass : NSObject

@property(nonatomic,strong) NSString *title;
@property(nonatomic,strong) NSString *message;
@property(nonatomic,strong) NSString *statusCode;
@property(nonatomic,strong) NSString *requestURL;
@property(nonatomic,strong) NSError *errorTipe;

-(instancetype)initWithURLDataTask:(NSURLSessionDataTask*)task;

@end

NS_ASSUME_NONNULL_END
