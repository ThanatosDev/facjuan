//
//  FACHttpClientUsage.h
//  FacCymetria
//
//  Created by Mac-01 on 20/11/18.
//  Copyright © 2018 JuanSoto. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "errorObjectClass.h"
#import "UserDataObjectClass.h"

NS_ASSUME_NONNULL_BEGIN

@interface FACHttpClientUsage : NSObject

//Get all Public Users from APO
+ (void)getUsersWithEmail: (NSString*)email AndBlock:(void (^)(UserDataObjectClass *usersPkg, errorObjectClass *error))block;
+ (void)createUsersWithUserName: (NSString*)userName userEmail: (NSString*)userMail identifier: (NSString*)identidentifier phone: (NSString*)phone address: (NSString*)address website: (NSString*)website gender:(NSString*)gender AndBlock:(void (^)(NSDictionary *usersDic, errorObjectClass *errorObj))block;


@end

NS_ASSUME_NONNULL_END
