//
//  FACHttpClient.m
//  FacCymetria
//
//  Created by Mac-01 on 20/11/18.
//  Copyright © 2018 JuanSoto. All rights reserved.
//

#import "FACHttpClient.h"

@implementation FACHttpClient

+ (FACHttpClient *)sharedFAC_HTTPClient{
    
    static FACHttpClient *_sharedFAC_HTTPClient = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _sharedFAC_HTTPClient = [[self alloc] initWithBaseURLRef:[NSURL URLWithString:FkApiRestBaseUrl]];
    });
    
    return _sharedFAC_HTTPClient;
}

- (instancetype)initWithBaseURLRef:(NSURL *)url{
    self = [super initWithBaseURL:url];
    
    if (self) {
        //        self.responseSerializer = [AFJSONResponseSerializer serializer];
        self.responseSerializer = [AFJSONResponseSerializer serializer];
        self.requestSerializer = [AFJSONRequestSerializer serializer];
        [self.requestSerializer setTimeoutInterval:180];
    }
    
    return self;
}

- (void)addTokenAuthorization:(NSString *)token{
    NSString *bearer = [NSString stringWithFormat:@"Bearer %@", token];
    [[FACHttpClient sharedFAC_HTTPClient].requestSerializer setValue:bearer forHTTPHeaderField:@"Authorization"];
}

@end
