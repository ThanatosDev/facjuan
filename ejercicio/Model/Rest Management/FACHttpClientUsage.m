//
//  FACHttpClientUsage.m
//  FacCymetria
//
//  Created by Mac-01 on 20/11/18.
//  Copyright © 2018 JuanSoto. All rights reserved.
//

#import "FACHttpClientUsage.h"
#import "FACHttpClient.h"
#import "Util.h"



@implementation FACHttpClientUsage


#pragma Mark -API Version

+(NSString*) getUrlPathWithAPIVersion:(NSString*)APIVersion andPath:(NSString *)URLPath{
    NSString*url=[NSString stringWithFormat:@"/%@/%@",APIVersion,URLPath];
    NSLog(@"Url Path: %@",url);
    return url;
}

#pragma Mark -JSON User Public API
+ (void)getUsersWithEmail: (NSString*)email AndBlock:(void (^)(UserDataObjectClass *, errorObjectClass *))block{
    
    NSString* userTkn= [Util getAPIAuthorizationToken];
    NSString* path= [NSString stringWithFormat:@"users?email=%@",email];
    
    [[FACHttpClient sharedFAC_HTTPClient] addTokenAuthorization:userTkn];
    [[FACHttpClient sharedFAC_HTTPClient] GET:[FACHttpClientUsage getUrlPathWithAPIVersion:FkApiVersion_Public andPath:path]
                                   parameters:nil
                                     progress:^(NSProgress * _Nonnull downloadProgress) {
                                         //Bloque progress
                                         NSLog(@"progress %@",downloadProgress);
                                         
                                     } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                         NSLog(@"Response :%@",responseObject);
                                         if ([responseObject[@"result"] count] > 0) {
                                             UserDataObjectClass *aUserObject = [[UserDataObjectClass alloc]initWithAttributes:responseObject[@"result"][0]];
                                             
                                             if(block)
                                                 block(aUserObject,nil);
                                         }else{
                                             errorObjectClass *errorObj= [[errorObjectClass alloc]init];
                                             errorObj.message=@"No se encuentra este email";
                                             errorObj.title =@"email";
                                             if(block)
                                                 block(nil,errorObj);
                                         }
                                         
                                         
                                     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                         NSLog(@"TASK::: %@",task.response);
                                         errorObjectClass *errorObj= [[errorObjectClass alloc]initWithURLDataTask:task];
                                         if(block)
                                             block(nil,errorObj);
                                     }];
}

+ (void)createUsersWithUserName: (NSString*)userName userEmail: (NSString*)userMail identifier: (NSString*)identidentifier phone: (NSString*)phone address: (NSString*)address website: (NSString*)website gender: (NSString*)gender AndBlock:(void (^)(NSDictionary *usersDic, errorObjectClass *errorObj))block{
    
    //    NSString* usersDic= [Util getAPIAuthorizationToken];
    NSDictionary *userData = [[NSDictionary alloc]initWithObjectsAndKeys:
                              userName,@"first_name",
                              userMail,@"email",
                              identidentifier,@"last_name",
                              gender,@"gender",
                              address,@"address",
                              phone,@"phone",
                              website,@"website",
                              nil];
    NSString* userTkn= [Util getAPIAuthorizationToken];
    
    [[FACHttpClient sharedFAC_HTTPClient] addTokenAuthorization:userTkn];
    
    [[FACHttpClient sharedFAC_HTTPClient] POST:[FACHttpClientUsage getUrlPathWithAPIVersion:FkApiVersion_Public andPath:@"users"]
                                    parameters:userData
                                      progress:^(NSProgress * _Nonnull downloadProgress) {
                                          //Bloque progress
                                          NSLog(@"progress %@",downloadProgress);
                                          
                                      } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                          NSLog(@"Response :%@",responseObject);
                                          NSLog(@"Response :%@",responseObject[@"_meta"][@"code"]);
                                          NSString *code= [NSString stringWithFormat:@"%@",responseObject[@"_meta"][@"code"]];
//algo
                                          if ([code  isEqualToString: @"401"]){
                                              
                                          if(block)
                                              block(responseObject,nil);
                                              
                                          }else{
                                                  NSLog(@"TASK::: %@",task.response);

                                                  errorObjectClass *errorObj= [[errorObjectClass alloc]initWithURLDataTask:task];
                                              errorObj.title=@"Email ya existe";
                                              errorObj.message=@"El email que esta registrando ya existe";
                                                  if(block)
                                                      block(nil,errorObj);
                                              }
                                        
                                      } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                          NSLog(@"FAIL::::::");
                                      }];
}


@end
