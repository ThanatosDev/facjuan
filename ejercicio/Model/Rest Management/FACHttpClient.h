//
//  FACHttpClient.h
//  FacCymetria
//
//  Created by Mac-01 on 20/11/18.
//  Copyright © 2018 JuanSoto. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPSessionManager.h"
#import "GlobalInstances.h"

NS_ASSUME_NONNULL_BEGIN

@interface FACHttpClient : AFHTTPSessionManager

+ (FACHttpClient *)sharedFAC_HTTPClient;
- (instancetype)initWithBaseURLRef:(NSURL *)url;
- (void)addTokenAuthorization:(NSString *)token;

@end

NS_ASSUME_NONNULL_END

