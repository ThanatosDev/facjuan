//
//  PostsDataObjectClass.m
//  FacCymetria
//
//  Created by Mac-01 on 20/11/18.
//  Copyright © 2018 JuanSoto. All rights reserved.
//

#import "PostDataObjectClass.h"

@implementation PostDataObjectClass

- (instancetype)initWithAttributes:(NSDictionary *)attr{
    self = [super init];
    
    if(self){
        self.identification = attr[FkRestUserDataPostid];
        self.identificationUser = attr[FkRestUserDataPostUserId];
        self.tittle= attr[FkRestUserDataPostTittle];
        self.body= attr[FkRestUserDataPostBody];
        
    }
    return self;

}

@end
