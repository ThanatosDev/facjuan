//
//  UserDataObjectClass.h
//  FacCymetria
//
//  Created by Mac-01 on 19/11/18.
//  Copyright © 2018 JuanSoto. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserDataLinksObjectClass.h"
#import "PostDataObjectClass.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserDataObjectClass : NSObject

@property(nonatomic,strong) NSString *identification;
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *gender;
@property(nonatomic,strong) NSString *dob;
@property(nonatomic,strong) NSString *email;
@property(nonatomic,strong) NSString *phone;
@property(nonatomic,strong) NSString *website;
@property(nonatomic,strong) NSString *address;
@property(nonatomic,strong) NSString *status;
@property(nonatomic,strong) UserDataLinksObjectClass *links;
@property(nonatomic,strong) NSMutableArray *posts;


- (instancetype)initWithAttributes:(NSDictionary *)attr;

@end

NS_ASSUME_NONNULL_END
