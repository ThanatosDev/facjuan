//
//  UserDataLinksObjectClass.h
//  FacCymetria
//
//  Created by Mac-01 on 19/11/18.
//  Copyright © 2018 JuanSoto. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserDataLinksSelf.h"
#import "UserDataLinksAvatar.h"


NS_ASSUME_NONNULL_BEGIN

@interface UserDataLinksObjectClass : NSObject

@property(nonatomic,strong) UserDataLinksSelf *selfa;
@property(nonatomic,strong) UserDataLinksAvatar *avatar;


- (instancetype)initWithAttributes:(NSDictionary *)attr;
@end

NS_ASSUME_NONNULL_END
