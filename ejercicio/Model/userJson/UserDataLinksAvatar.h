//
//  UserDataLinksAvatar.h
//  FacCymetria
//
//  Created by Mac-01 on 19/11/18.
//  Copyright © 2018 JuanSoto. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserDataLinksAvatar : NSObject

@property(nonatomic,strong) NSString *href;


- (instancetype)initWithAttributes:(NSDictionary *)attr;

@end

NS_ASSUME_NONNULL_END
