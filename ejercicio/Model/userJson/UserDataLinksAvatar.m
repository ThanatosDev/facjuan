//
//  UserDataLinksAvatar.m
//  FacCymetria
//
//  Created by Mac-01 on 19/11/18.
//  Copyright © 2018 JuanSoto. All rights reserved.
//

#import "UserDataLinksAvatar.h"
#import "GlobalInstances.h"

@implementation UserDataLinksAvatar
- (instancetype)initWithAttributes:(NSDictionary *)attr{
    
    self = [super init];
    
    if(self){
        
        self.href= attr[FkRestUserDataLinksAvatarHref];
        
    }
    return self;
}
@end
