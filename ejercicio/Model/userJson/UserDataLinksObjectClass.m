//
//  UserDataLinksObjectClass.m
//  FacCymetria
//
//  Created by Mac-01 on 19/11/18.
//  Copyright © 2018 JuanSoto. All rights reserved.
//

#import "UserDataLinksObjectClass.h"
#import "GlobalInstances.h"

@implementation UserDataLinksObjectClass
- (instancetype)initWithAttributes:(NSDictionary *)attr{
    
    self = [super init];
    
    if(self){
        
        self.selfa= [[UserDataLinksSelf alloc]initWithAttributes:attr[FkRestUserDataLinksSelf]];
        self.avatar= [[UserDataLinksAvatar alloc]initWithAttributes:attr[FkRestUserDataLinksAvatar]];

        
    }
    return self;
}
@end
