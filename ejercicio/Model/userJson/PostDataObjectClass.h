//
//  PostsDataObjectClass.h
//  FacCymetria
//
//  Created by Mac-01 on 20/11/18.
//  Copyright © 2018 JuanSoto. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GlobalInstances.h"

NS_ASSUME_NONNULL_BEGIN

@interface PostDataObjectClass : NSObject

@property(nonatomic,strong) NSString *identification;
@property(nonatomic,strong) NSString *identificationUser;
@property(nonatomic,strong) NSString *tittle;
@property(nonatomic,strong) NSString *body;


- (instancetype)initWithAttributes:(NSDictionary *)attr;

@end

NS_ASSUME_NONNULL_END
