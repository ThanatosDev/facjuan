//
//  UserDataObjectClass.m
//  FacCymetria
//
//  Created by Mac-01 on 19/11/18.
//  Copyright © 2018 JuanSoto. All rights reserved.
//

#import "UserDataObjectClass.h"
#import "GlobalInstances.h"


@implementation UserDataObjectClass

- (instancetype)initWithAttributes:(NSDictionary *)attr{
    self = [super init];
    
    if(self){
        self.identification= attr[FkRestUserDataId];
        self.name= attr[FkRestUserDataName];
        self.gender= attr[FkRestUserDataGender];
        self.dob= attr[FkRestUserDataDob];
        self.email= attr[FkRestUserDataEmail];
        self.phone= attr[FkRestUserDataPhone];
        self.website= attr[FkRestUserDataWebsite];
        self.address= attr[FkRestUserDataAddress];
        self.status= attr[FkRestUserDataStatus];
        self.links = [[UserDataLinksObjectClass alloc]initWithAttributes:attr[FkRestUserDataLinks]];
        self.posts = [self createPostsWithArray:attr[FkRestUserDataPost]];

    }
    return self;
    
}

-(NSMutableArray *)createPostsWithArray:(NSArray*)arr{
    NSMutableArray *posts = [NSMutableArray array];
    for (NSDictionary *post in arr) {
        PostDataObjectClass *aUserObject = [[PostDataObjectClass alloc]initWithAttributes:post];
        [posts addObject:aUserObject];
        
    }
    return posts;
}

@end
