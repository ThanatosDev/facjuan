//
//  ActivityIndicatorBohra.h
//  Bohra
//
//  Created by Camilo Zambrano on 8/23/14.
//  Copyright (c) 2014 Camili Zambrano. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityIndicatorBohra : UIView

-(void)show;
-(void)hide;

@end
