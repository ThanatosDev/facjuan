//
//  ActivityIndicatorClass.h
//  FacCymetria
//
//  Created by Mac-10 on 23/11/18.
//  Copyright © 2018 JuanSoto. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol activityFACDelegate <NSObject>

-(void)activityFACViewDissmised;

@end


@interface ActivityIndicatorClass : UIView<UIGestureRecognizerDelegate>
{
    id<activityFACDelegate> delegate;
}

@property(nonatomic) id<activityFACDelegate> delegate;

-(void)startAnimating;
-(void)stopAnimating;
+ (ActivityIndicatorClass *)sharedActivityIndicator;

@end

NS_ASSUME_NONNULL_END
