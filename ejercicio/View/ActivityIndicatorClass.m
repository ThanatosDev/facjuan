//
//  ActivityIndicatorClass.m
//  FacCymetria
//
//  Created by Mac-10 on 23/11/18.
//  Copyright © 2018 JuanSoto. All rights reserved.
//


#import "ActivityIndicatorClass.h"

@implementation ActivityIndicatorClass{
    UIImageView *spin;
    CGRect mainFrame;
    UIImageView *background;
}

+ (ActivityIndicatorClass *)sharedActivityIndicator{
    
    static ActivityIndicatorClass *_sharedActivityIndicator = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _sharedActivityIndicator = [[self alloc] init];
    });
    
    return _sharedActivityIndicator;
}

- (instancetype)init{
    self = [super init];
    
    if (self) {
        [self initialize];
    }
    
    return self;
}

@synthesize delegate;

- (void)prepare{
    //    static MijemActivitIndicator *_prepare = nil;
    self.frame = [UIApplication sharedApplication].keyWindow.bounds;
    [self initialize];
}

-(void)initialize{
    self.alpha =0;
    mainFrame = [UIApplication sharedApplication].keyWindow.bounds;
    UIImageView *image=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0,60, 60)];
    [image setCenter:CGPointMake(mainFrame.size.width/2, mainFrame.size.height/2)];
    image.backgroundColor=[UIColor clearColor];
    image.layer.cornerRadius=30;
    image.layer.masksToBounds=YES;
    image.backgroundColor=[UIColor colorWithRed:235/255.0 green:235/255.0 blue:235/255.0 alpha:1];
    
    background=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, mainFrame.size.width, mainFrame.size.height)];
    background.backgroundColor=[UIColor grayColor];
    background.alpha=0.5;
    background.userInteractionEnabled=TRUE;
    
    spin=[[UIImageView alloc]initWithFrame:CGRectMake(0, 100, 30, 30)];
    [spin setCenter:CGPointMake(image.frame.size.width/2, image.frame.size.height/2)];
    spin.image=[UIImage imageNamed:@"loading2x.png"];
    
    UIVisualEffect *blurEffect;
    blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
    
    UIVisualEffectView *visualEffectView;
    visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    visualEffectView.frame = image.bounds;
    
    [image addSubview:visualEffectView];
    [self addSubview:background];
    [self addSubview: image];
    [image addSubview:spin];
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped)];
    [tapRecognizer setNumberOfTapsRequired:1];
    [tapRecognizer setDelegate:self];
    
    
    [background addGestureRecognizer:tapRecognizer];
    
}
-(void)tapped{
    NSLog(@"tttaapp");
}

- (void)rotate360WithDuration:(CGFloat)duration repeatCount:(float)repeatCount{
    CABasicAnimation *fullRotation;
    fullRotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    fullRotation.fromValue = [NSNumber numberWithFloat:0];
    fullRotation.toValue = [NSNumber numberWithFloat:((360 * M_PI) / 180)];
    fullRotation.duration = duration;
    if (repeatCount == 0)
        fullRotation.repeatCount = MAXFLOAT;
    else
        fullRotation.repeatCount = repeatCount;
    
    [spin.layer addAnimation:fullRotation forKey:@"360"];
}

- (void)stopAllAnimations{
    [spin.layer removeAllAnimations];
};

- (void)pauseAnimations{
    [self pauseLayer:spin.layer];
}

- (void)resumeAnimations{
    [self resumeLayer:spin.layer];
}

-(void)startAnimating{
    NSLog(@"Animating previous");
    [self rotate360WithDuration:1.3 repeatCount:0];
    self.userInteractionEnabled=YES;
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [self showActivity];
}

-(void)stopAnimating{
    self.userInteractionEnabled=NO;
    //    [self stopAllAnimations];
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    [self hideActivity];
    
}

- (void)pauseLayer:(CALayer *)layer{
    CFTimeInterval pausedTime = [layer convertTime:CACurrentMediaTime() fromLayer:nil];
    layer.speed = 0.0;
    layer.timeOffset = pausedTime;
}

- (void)resumeLayer:(CALayer *)layer{
    CFTimeInterval pausedTime = [layer timeOffset];
    layer.speed = 1.0;
    layer.timeOffset = 0.0;
    layer.beginTime = 0.0;
    CFTimeInterval timeSincePause = [layer convertTime:CACurrentMediaTime() fromLayer:nil] - pausedTime;
    layer.beginTime = timeSincePause;
}

#pragma mark - View Animation Management

-(void)showActivity{
    [[UIApplication sharedApplication].keyWindow bringSubviewToFront:self];
    [UIView animateWithDuration:0.5 animations:^{
        self.alpha=1;
    }];
    NSLog(@"Animating started");
}

-(void)hideActivity{
    [UIView animateWithDuration:0.5 animations:^{
        self.alpha=0;
    }];
    NSLog(@"Animating end");
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSLog(@"Touches began");
    
}


/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end

