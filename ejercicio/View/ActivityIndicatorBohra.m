//
//  ActivityIndicatorBohra.m
//  Bohra
//
//  Created by Camilo Zambrano on 8/23/14.
//  Copyright (c) 2014 Camili Zambrano. All rights reserved.
//

#import "ActivityIndicatorBohra.h"

@implementation ActivityIndicatorBohra
{
    UIImageView *blackActivityView;
    UIImageView *activityBlackSquare;
    UIActivityIndicatorView *activityIndicatorDefault;
    
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self createActivityIndicatorViewWithFrame:frame];
    }
    return self;
}
-(void)createActivityIndicatorViewWithFrame:(CGRect)frame
{
    
    blackActivityView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height+200)];
    blackActivityView.backgroundColor=[UIColor blackColor];
    blackActivityView.alpha=0;
    blackActivityView.userInteractionEnabled=YES;
    
    activityBlackSquare=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
    activityBlackSquare.backgroundColor=[UIColor blackColor];
    activityBlackSquare.layer.cornerRadius=10;
    activityBlackSquare.alpha=0;
    activityBlackSquare.layer.masksToBounds=YES;
    [activityBlackSquare setCenter:CGPointMake(frame.size.width/2, frame.size.height/2)];
    
    activityIndicatorDefault=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [activityIndicatorDefault setCenter:CGPointMake(frame.size.width/2,frame.size.height/2)];
    
    [self addSubview:blackActivityView];
    [self addSubview:activityBlackSquare];
    [self addSubview:activityIndicatorDefault];
    self.userInteractionEnabled=NO;
    
}


#pragma mark - View Animation Management


-(void)show
{
    self.userInteractionEnabled=YES;
    [UIView animateWithDuration:0.5 animations:^
     {
         self->activityBlackSquare.alpha=0.7;
         self->blackActivityView.alpha=0.2;
     }];
    [activityIndicatorDefault startAnimating];
    
}

-(void)hide
{
    self.userInteractionEnabled=NO;
    [UIView animateWithDuration:0.5 animations:^
     {
         self->activityBlackSquare.alpha=0;
         self->blackActivityView.alpha=0;
     }];
    [activityIndicatorDefault stopAnimating];
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
