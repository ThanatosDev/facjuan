//
//  Util.h
//  FacCymetria
//
//  Created by Mac-01 on 19/11/18.
//  Copyright © 2018 JuanSoto. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Util : NSObject

+(NSDictionary *)getJeisonResponseWithString:(NSString*)stringURL;


#pragma Mark -API Authorization Token
+(void)saveAPIAuthorizationTokenWithToken:(NSString*)token;
+(NSString *)getAPIAuthorizationToken;

#pragma Mark -RootTabbarController
+(UITabBarController*)getMainUITabBarController;

#pragma Mark -ValidEmail
+ (BOOL) isValidEmail:(NSString *)aString;

#pragma Mark -Aplication Colors
+(UIColor*) getMaintextFieldBackground;

+(NSAttributedString*)getAtributedStringWithText:(NSString*)text andColor:(UIColor*)color;


@end

NS_ASSUME_NONNULL_END
