//
//  Util.m
//  FacCymetria
//
//  Created by Mac-01 on 19/11/18.
//  Copyright © 2018 JuanSoto. All rights reserved.
//

#import "Util.h"
#import "GlobalInstances.h"
#import "ProfileViewController.h"
#import "SettingsViewController.h"

@implementation Util

+(NSDictionary *)getJeisonResponseWithString:(NSString*)stringURL{
    NSError *error;
    NSString *url_string = [NSString stringWithFormat: @"%@", stringURL];
    NSData *data = [NSData dataWithContentsOfURL: [NSURL URLWithString:url_string]];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    //NSLog(@"json: %@", json);
    
    return json;
}

#pragma Mark -API Authorization Token

+(void)saveAPIAuthorizationTokenWithToken:(NSString*)token{
    [[NSUserDefaults standardUserDefaults]setObject:token forKey:FkApiTokenKey];
}


+(NSString *)getAPIAuthorizationToken{
    return [[NSUserDefaults standardUserDefaults]objectForKey:FkApiTokenKey];
}

+(UITabBarController*)getMainUITabBarController{
    ProfileViewController *firstViewController = [[ProfileViewController alloc]init];
    firstViewController.tabBarItem = [[UITabBarItem alloc]initWithTabBarSystemItem:UITabBarSystemItemSearch tag:0];
    UINavigationController *firstNavController = [[UINavigationController alloc]initWithRootViewController:firstViewController];
    
    SettingsViewController *secondViewController = [[SettingsViewController alloc]init];
    secondViewController.tabBarItem = [[UITabBarItem alloc]initWithTabBarSystemItem:UITabBarSystemItemFavorites tag:1];
    UINavigationController *secondNavController = [[UINavigationController alloc]initWithRootViewController:secondViewController];
    
    UITabBarController *tabBarvc = [[UITabBarController alloc] initWithNibName:nil bundle:nil];
    tabBarvc.viewControllers = [[NSArray alloc] initWithObjects:firstNavController, secondNavController ,nil];
    
    return tabBarvc;
}


#pragma Mark -ValidEmail

+ (BOOL) isValidEmail:(NSString *)aString{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:aString];
}

#pragma Mark -Aplication Colors
+(UIColor*) getMaintextFieldBackground{
    UIColor *backgroundColorTextField = [UIColor redColor];
    return backgroundColorTextField;
}


+(NSAttributedString*)getAtributedStringWithText:(NSString*)text andColor:(UIColor*)color{
    NSDictionary *attrs= @{NSForegroundColorAttributeName: color};
    NSAttributedString *attrStr= [[NSAttributedString alloc]initWithString:text attributes:attrs];
    return attrStr;
}

@end
