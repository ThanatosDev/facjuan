//
//  ProfileViewController.m
//  ejercicio
//
//  Created by Mac-01 on 24/11/18.
//  Copyright © 2018 JuanSoto. All rights reserved.
//

#import "ProfileViewController.h"
#import "GlobalInstances.h"
#import "AFNetworkReachabilityManager.h"
#import "FACHttpClientUsage.h"
#import "Util.h"
//:::::::::::::::prueba::::::::::::::::
@interface ProfileViewController (){
    ActivityIndicatorClass *activityIndicatorClass;
    UserDataObjectClass *userContent;
    UITableView *profileTableView;
}

@end

@implementation ProfileViewController

#pragma mark- Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initialConfigurations];
    [self uiManagement];

    
}

-(void)viewDidAppear:(BOOL)animated{
    
}
-(void)viewWillAppear:(BOOL)animated{
    
}
-(void)viewWillDisappear:(BOOL)animated{
    
}

#pragma mark- Initial Configurations
-(void)initialConfigurations{
    [self initialAPIRequest];
}

-(void)ShowRestAlertWithTitle: (NSString*)title message:(NSString*)message{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              NSLog(@"OK");
                                                              
                                                          }];
    
    
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark- View Management
-(void)uiManagement{
    [self createUITableView];
    
    
}

-(void)createUITableView {
    profileTableView = [[UITableView alloc] init];
    profileTableView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    profileTableView.delegate = self;
    profileTableView.dataSource = self;

    
    
    [self.view addSubview:profileTableView];
}

#pragma mark- UITableViewDelegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)theTableView
{
    return 1;
}

// number of row in the section, I assume there is only 1 row
- (NSInteger)tableView:(UITableView *)theTableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"newFriendCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    if (indexPath.row == 0) {
        
        UIImageView *ProfileCoverImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height/4)];
        ProfileCoverImage.backgroundColor=[UIColor redColor];
        [ProfileCoverImage setImage:[UIImage imageNamed:@"profileCover.jpg"]];

        
        UIImageView *ProfilePicture=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 150, 150)];
        ProfilePicture.center = CGPointMake(self.view.frame.size.width/2, ProfileCoverImage.frame.size.height);
        ProfilePicture.backgroundColor=[UIColor redColor];
        ProfilePicture.layer.cornerRadius=ProfilePicture.frame.size.width/2;
        [ProfilePicture setImage:[UIImage imageNamed:@"profile.png"]];
        ProfilePicture.layer.masksToBounds=YES;
        ProfilePicture.layer.borderWidth = 5;
        ProfilePicture.layer.borderColor = [[UIColor whiteColor]CGColor];

        UILabel *ProfileNameLabel = [[UILabel alloc] init];
        ProfileNameLabel.frame = CGRectMake(0, 0, 500, 50);
        ProfileNameLabel.center = CGPointMake(ProfileCoverImage.frame.size.width/2, ProfileCoverImage.frame.size.height/2-30);
        ProfileNameLabel.text = userContent.name;
        ProfileNameLabel.adjustsFontSizeToFitWidth = YES;
        ProfileNameLabel.textColor = [UIColor redColor];
        ProfileNameLabel.textAlignment = NSTextAlignmentCenter;
        ProfileNameLabel.userInteractionEnabled=NO;
        ProfileNameLabel.font=[UIFont fontWithName:@"Arial" size:30];

        
        
        UILabel *ProfessionNameLabel = [[UILabel alloc] init];
        ProfessionNameLabel.frame = CGRectMake(0, 0, 500, 50);
        ProfessionNameLabel.center = CGPointMake(ProfileCoverImage.frame.size.width/2, ProfileCoverImage.frame.size.height/2);
        ProfessionNameLabel.text = userContent.gender;
        ProfessionNameLabel.adjustsFontSizeToFitWidth = YES;
        ProfessionNameLabel.textColor = [UIColor redColor];
        ProfessionNameLabel.textAlignment = NSTextAlignmentCenter;
        ProfessionNameLabel.userInteractionEnabled=NO;
        
        UILabel *NumberOfPostsLabel = [[UILabel alloc] init];
        NumberOfPostsLabel.frame = CGRectMake(0, 0, 100, 20);
        NumberOfPostsLabel.center = CGPointMake(ProfileCoverImage.frame.size.width/2-150, ProfilePicture.frame.origin.y+160);
        NumberOfPostsLabel.text = userContent.identification;
        NumberOfPostsLabel.adjustsFontSizeToFitWidth = YES;
        NumberOfPostsLabel.textColor = [UIColor blackColor];
        NumberOfPostsLabel.textAlignment = NSTextAlignmentCenter;
        NumberOfPostsLabel.userInteractionEnabled=NO;
        
        UILabel *PostsLabel = [[UILabel alloc] init];
        PostsLabel.frame = CGRectMake(0, 0, 100, 50);
        PostsLabel.center = CGPointMake(ProfileCoverImage.frame.size.width/2-150, ProfilePicture.frame.origin.y+180);
        PostsLabel.text = @"Posts";
        PostsLabel.adjustsFontSizeToFitWidth = YES;
        PostsLabel.textColor = [UIColor blackColor];
        PostsLabel.textAlignment = NSTextAlignmentCenter;
        PostsLabel.userInteractionEnabled=NO;
        
        UILabel *NumberOfFollowersLabel = [[UILabel alloc] init];
        NumberOfFollowersLabel.frame = CGRectMake(0, 0, 100, 50);
        NumberOfFollowersLabel.center = CGPointMake(ProfileCoverImage.frame.size.width/2, ProfilePicture.frame.origin.y+160);
        NumberOfFollowersLabel.text = userContent.phone;
        NumberOfFollowersLabel.adjustsFontSizeToFitWidth = YES;
        NumberOfFollowersLabel.textColor = [UIColor blackColor];
        NumberOfFollowersLabel.textAlignment = NSTextAlignmentCenter;
        NumberOfFollowersLabel.userInteractionEnabled=NO;
        
        UILabel *FollowersLabel = [[UILabel alloc] init];
        FollowersLabel.frame = CGRectMake(0, 0, 500, 50);
        FollowersLabel.center = CGPointMake(ProfileCoverImage.frame.size.width/2, ProfilePicture.frame.origin.y+180);
        FollowersLabel.text = userContent.email;
        FollowersLabel.adjustsFontSizeToFitWidth = YES;
        FollowersLabel.textColor = [UIColor blackColor];
        FollowersLabel.textAlignment = NSTextAlignmentCenter;
        FollowersLabel.userInteractionEnabled=NO;
        
        UILabel *NumberOfFollowingLabel = [[UILabel alloc] init];
        NumberOfFollowingLabel.frame = CGRectMake(0, 0, 500, 50);
        NumberOfFollowingLabel.center = CGPointMake(ProfileCoverImage.frame.size.width/2+150, ProfilePicture.frame.origin.y+160);
        NumberOfFollowingLabel.text = @"563";
        NumberOfFollowingLabel.adjustsFontSizeToFitWidth = YES;
        NumberOfFollowingLabel.textColor = [UIColor blackColor];
        NumberOfFollowingLabel.textAlignment = NSTextAlignmentCenter;
        NumberOfFollowingLabel.userInteractionEnabled=NO;
        
        UILabel *FollowingLabel = [[UILabel alloc] init];
        FollowingLabel.frame = CGRectMake(0, 0, 500, 50);
        FollowingLabel.center = CGPointMake(ProfileCoverImage.frame.size.width/2+150, ProfilePicture.frame.origin.y+180);
        FollowingLabel.text = @"Following";
        FollowingLabel.adjustsFontSizeToFitWidth = YES;
        FollowingLabel.textColor = [UIColor blackColor];
        FollowingLabel.textAlignment = NSTextAlignmentCenter;
        FollowingLabel.userInteractionEnabled=NO;
        
        UIButton *followingBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 250, 50)];
        followingBtn.center=CGPointMake(self.view.frame.size.width/2, ProfilePicture.frame.origin.y+220);
        followingBtn.backgroundColor=[UIColor greenColor];
        [followingBtn addTarget:self action:@selector(signInBtnSerlector:) forControlEvents:UIControlEventTouchUpInside];
        [followingBtn setTitle:@"Following" forState:UIControlStateNormal];
        
        [cell addSubview:ProfileCoverImage];
        [cell addSubview:ProfilePicture];
        [cell addSubview:ProfileNameLabel];
        [cell addSubview:ProfessionNameLabel];
        [cell addSubview:NumberOfPostsLabel];
        [cell addSubview:PostsLabel];
        [cell addSubview:NumberOfFollowersLabel];
        [cell addSubview:FollowersLabel];
        [cell addSubview:NumberOfFollowingLabel];
        [cell addSubview:FollowingLabel];
        [cell addSubview:followingBtn];

    }else{
        
        UIImageView *ProfilePicture=[[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 40, 40)];
        ProfilePicture.backgroundColor=[UIColor redColor];
        ProfilePicture.layer.cornerRadius=ProfilePicture.frame.size.width/2;
        [ProfilePicture setImage:[UIImage imageNamed:@"profile.png"]];
        ProfilePicture.layer.masksToBounds=YES;

        
        UILabel *ProfileNameLabel = [[UILabel alloc] init];
        ProfileNameLabel.frame = CGRectMake(60, 10, 100, 20);
        ProfileNameLabel.text = FkRestUserName;
        ProfileNameLabel.adjustsFontSizeToFitWidth = YES;
        ProfileNameLabel.textColor = [UIColor blackColor];
        ProfileNameLabel.textAlignment = NSTextAlignmentLeft;
        ProfileNameLabel.userInteractionEnabled=NO;
        
        UILabel *NumberOfViewsLabel = [[UILabel alloc] init];
        NumberOfViewsLabel.frame = CGRectMake(70, 30, 100, 20);
        NumberOfViewsLabel.text = @"15";
        NumberOfViewsLabel.adjustsFontSizeToFitWidth = YES;
        NumberOfViewsLabel.textColor = [UIColor grayColor];
        NumberOfViewsLabel.textAlignment = NSTextAlignmentLeft;
        NumberOfViewsLabel.font=[UIFont fontWithName:@"Arial" size:12];
        NumberOfViewsLabel.userInteractionEnabled=NO;
        
        UILabel *PostTime = [[UILabel alloc] init];
        PostTime.frame = CGRectMake(self.view.frame.size.width-60, 30, 100, 20);
        PostTime.text = @"11:57 AM";
        PostTime.adjustsFontSizeToFitWidth = YES;
        PostTime.textColor = [UIColor grayColor];
        PostTime.textAlignment = NSTextAlignmentLeft;
        PostTime.font=[UIFont fontWithName:@"Arial" size:12];
        PostTime.userInteractionEnabled=NO;
        
        UILabel *contentTableLbl = [[UILabel alloc] init];
        contentTableLbl.frame = CGRectMake(10, 70, self.view.frame.size.width, 60);
        contentTableLbl.text = @"dhfjsd asjhdjklashdj asjdhajsd asdhjkl hjasgdhjasdb hhasdasd hjsbdahjs bjasdbalsjd asdjasd asdhjasd hjasdas hjksadhg ashjkhdh jkasdha sajbhdasbdj asdnjkln";
        contentTableLbl.adjustsFontSizeToFitWidth = YES;
        contentTableLbl.textColor = [UIColor grayColor];
        contentTableLbl.textAlignment = NSTextAlignmentLeft;
        contentTableLbl.userInteractionEnabled=NO;
        contentTableLbl.numberOfLines = 3;
        contentTableLbl.adjustsFontForContentSizeCategory = YES;
        
        UIImageView *PostPicture=[[UIImageView alloc]initWithFrame:CGRectMake(0, 150, self.view.frame.size.width, 300)];
        PostPicture.backgroundColor=[UIColor redColor];
        [PostPicture setImage:[UIImage imageNamed:@"postImages.jpg"]];
        PostPicture.layer.borderWidth = 5;
        PostPicture.layer.borderColor = [[UIColor whiteColor]CGColor];
        
        [cell addSubview:ProfilePicture];
        [cell addSubview:ProfileNameLabel];
        [cell addSubview:NumberOfViewsLabel];
        [cell addSubview:PostTime];
        [cell addSubview:contentTableLbl];
        [cell addSubview:PostPicture];

        
    }
    //etc.
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    CGFloat heightForRow = 0;
    
    if (indexPath.section == 0 && indexPath.row == 0) {
        heightForRow = 400;
    }else{
        heightForRow = 450;

    }
    return heightForRow;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"row:%ld section:%ld ",(long)indexPath.row,indexPath.section);
    
}

#pragma mark- API Request
-(void)initialAPIRequest{
    
    [[ActivityIndicatorClass sharedActivityIndicator] startAnimating];
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        
        switch (status) {
            case AFNetworkReachabilityStatusNotReachable:
                NSLog(@"AFNetworkReachabilityStatusNotReachable");
                break;
            case AFNetworkReachabilityStatusReachableViaWiFi:
                NSLog(@"AFNetworkReachabilityStatusReachableViaWiFi");
                break;
            case AFNetworkReachabilityStatusReachableViaWWAN:
                NSLog(@"AFNetworkReachabilityStatusReachableViaWWAN");
                break;
            default:
                NSLog(@"Unkown network status");
                
        }
        [[ActivityIndicatorClass sharedActivityIndicator] stopAnimating];
        if ([[AFNetworkReachabilityManager sharedManager]isReachable]) {
            [self getUserInfo];
            
            
        }else{
            [self ShowRestAlertWithTitle:@"Lo Sentimos" message:@"No tienes una coneccion a internet."];
        }
        
        
    }];
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
}

-(void)getUserInfo{
    
    [[ActivityIndicatorClass sharedActivityIndicator]startAnimating];
    
    [FACHttpClientUsage getUsersWithEmail: [[NSUserDefaults standardUserDefaults]objectForKey:@"email"] AndBlock:^(UserDataObjectClass *  userObject, errorObjectClass *  error) {
        if (!error) {
            self->userContent=userObject;
            [self->profileTableView reloadData];
            [[ActivityIndicatorClass sharedActivityIndicator]stopAnimating];
            
        }else{
            [[ActivityIndicatorClass sharedActivityIndicator]stopAnimating];
            
            [self ShowRestAlertWithTitle:error.title message:error.message];
            
        }
    }];
}



@end
