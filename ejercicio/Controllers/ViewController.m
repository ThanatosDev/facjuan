//
//  ViewController.m
//  ejercicio
//
//  Created by Mac-01 on 28/11/18.
//  Copyright © 2018 JuanSoto. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)botonAction:(UIButton *)sender {
    [UIView animateWithDuration:0.5 animations:^{
        self->_profilePic.center=CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2);
    } completion:^(BOOL finished) {

    }];
    NSLog(@"botonAction");
}
@end
