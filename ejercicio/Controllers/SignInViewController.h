//
//  SignInViewController.h
//  ejercicio
//
//  Created by Mac-01 on 24/11/18.
//  Copyright © 2018 JuanSoto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignUpViewController.h"
#import "UserDataObjectClass.h"
#import "ProfileViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SignInViewController : UIViewController

@end

NS_ASSUME_NONNULL_END
