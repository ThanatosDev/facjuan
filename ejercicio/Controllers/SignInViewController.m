//
//  SignInViewController.m
//  ejercicio
//
//  Created by Mac-01 on 24/11/18.
//  Copyright © 2018 JuanSoto. All rights reserved.
//

#import "SignInViewController.h"
#import "AFNetworkReachabilityManager.h"
#import "FACHttpClientUsage.h"
#import "Util.h"


@interface SignInViewController (){
    UITextField *userNameTextField;
    UITextField *userPassTextField;
    UserDataObjectClass *userContent;

}

@end

@implementation SignInViewController

#pragma mark- Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initialConfigurations];
    //    [self initialAPIRequest];
}

-(void)viewDidAppear:(BOOL)animated{
    [self uiManagement];
    //    [self createActivityIndicator];
    
}
-(void)viewWillAppear:(BOOL)animated{
    
}
-(void)viewWillDisappear:(BOOL)animated{
    
}

#pragma mark- Initial Configurations
-(void)initialConfigurations{
    [self registerSignUpObserver];
}



-(void)ShowRestAlertWithTitle: (NSString*)title message:(NSString*)message{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              NSLog(@"OK");
                                                              
                                                          }];
    
    
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)registerSignUpObserver{
    // Register for SIGNUP notification when invalid access token
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleSignUpEvent)
                                                 name:@"SignUpEvent"
                                               object:nil];
    
}

-(void)handleSignUpEvent{
    NSLog(@"ENTRO AL OBSERVER::::::");
}

#pragma mark- View Management
-(void)uiManagement{
    [self createImagebackground];
    [self createLabelWithTitle:@"FAC" fontSize:50 coordinateX:self.view.frame.size.width/2 coordinateY:180 lblColor:[UIColor redColor]];
    [self createUsuarioUITextField];
    [self createPassUITextField];
    [self createSignUpBtn];
    [self createSignInBtn];

}

-(void)createImagebackground{
    UIImageView *backgroundImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    backgroundImage.backgroundColor=[UIColor redColor];
    backgroundImage.layer.cornerRadius=backgroundImage.frame.size.width/2;
    [backgroundImage setImage:[UIImage imageNamed:@"background.jpg"]];
    
    
    [self.view addSubview:backgroundImage];
}

-(void)createLabelWithTitle:(NSString *)titlelbl fontSize:(CGFloat)fontSize coordinateX:(CGFloat)coordinateX coordinateY:(CGFloat)coordinateY lblColor:(UIColor*)lblColor {
    //    UILabel *testLabel = [[UILabel alloc] initWithFrame:CGRectMake(91, 15,2, 1)];
    UILabel *testLabel = [[UILabel alloc] init];
    testLabel.frame = CGRectMake(coordinateX, coordinateY, self.view.frame.size.width, 70);
    testLabel.center = CGPointMake(coordinateX, coordinateY);
    testLabel.text = titlelbl;
    testLabel.numberOfLines = 3;
    //                          testLabel.baselineAdjustment = UIBaselineAdjustmentAlignBaselines; // or UIBaselineAdjustmentAlignCenters, or UIBaselineAdjustmentNone
    testLabel.adjustsFontSizeToFitWidth = NO;
    //    fromLabel.adjustsFontSizeToFitWidth = YES;
    //    testLabel.minimumScaleFactor = 10.0f/12.0f;
    testLabel.font=[UIFont fontWithName:@"Optima" size:fontSize];
    testLabel.clipsToBounds = YES;
    testLabel.backgroundColor = [UIColor colorWithRed:110.0f/255.0f green:159.0f/255.0f blue:224.0f/255.0f alpha:0];
    testLabel.textColor = [UIColor redColor];
    testLabel.textAlignment = NSTextAlignmentCenter;
    testLabel.userInteractionEnabled=NO;
    testLabel.textAlignment = NSTextAlignmentCenter;
    
    
    //    classIsActive=YES;
    [self.view addSubview:testLabel];
}

-(void)createUsuarioUITextField{
    
    userNameTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-60, 50)];
    userNameTextField.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2-60);
    userNameTextField.placeholder = @"usuario";
    userNameTextField.backgroundColor = [UIColor whiteColor];
    userNameTextField.textColor = [UIColor blackColor];
    userNameTextField.font = [UIFont systemFontOfSize:14.0f];
    userNameTextField.borderStyle = UITextBorderStyleRoundedRect;
    userNameTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    userNameTextField.returnKeyType = UIReturnKeyDone;
    userNameTextField.textAlignment = NSTextAlignmentLeft;
    userNameTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    userNameTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    userNameTextField.layer.borderColor=[[UIColor whiteColor]CGColor];
    
    [self.view addSubview:userNameTextField];
}

-(void)createPassUITextField{
    
    userPassTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-60, 50)];
    userPassTextField.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2);
    userPassTextField.placeholder = @"Password";
    userPassTextField.backgroundColor = [UIColor whiteColor];
    userPassTextField.textColor = [UIColor blackColor];
    userPassTextField.font = [UIFont systemFontOfSize:14.0f];
    userPassTextField.borderStyle = UITextBorderStyleRoundedRect;
    userPassTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    userPassTextField.returnKeyType = UIReturnKeyDone;
    userPassTextField.textAlignment = NSTextAlignmentLeft;
    userPassTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    userPassTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    userPassTextField.layer.borderColor=[[UIColor whiteColor]CGColor];
    
    [self.view addSubview:userPassTextField];
}

-(void)createSignInBtn{
    UIButton *createSignUpBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 140, 30)];
    createSignUpBtn.center=CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2+60);
    createSignUpBtn.backgroundColor=[UIColor redColor];
    [createSignUpBtn addTarget:self action:@selector(signInButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    //    [btnExample addTarget:self action:@selector(metodoBtnExampleDown:) forControlEvents:UIControlEventTouchDown];
    [createSignUpBtn setTitle:@"Sign In" forState:UIControlStateNormal];
    createSignUpBtn.layer.cornerRadius = createSignUpBtn.frame.size.height/2;

    [self.view addSubview:createSignUpBtn];
}

-(void)createSignUpBtn{
    UIButton *createSignUpBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 140, 30)];
    createSignUpBtn.center=CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height-100);
    createSignUpBtn.backgroundColor=[UIColor redColor];
    [createSignUpBtn addTarget:self action:@selector(signUpButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    //    [btnExample addTarget:self action:@selector(metodoBtnExampleDown:) forControlEvents:UIControlEventTouchDown];
    [createSignUpBtn setTitle:@"Sign Up" forState:UIControlStateNormal];
    
    [self.view addSubview:createSignUpBtn];
}


#pragma mark- UIButtonMethods

-(void)signUpButtonAction:(UIButton*)btn{
    
    SignUpViewController *add = [[SignUpViewController alloc] init];
    [self presentViewController:add animated:YES completion:nil];
}

-(void)signInButtonAction:(UIButton*)btn{
    
    [self initialAPIRequest];
}

#pragma mark- API Request
-(void)initialAPIRequest{
    
    [[ActivityIndicatorClass sharedActivityIndicator] startAnimating];
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        
        switch (status) {
            case AFNetworkReachabilityStatusNotReachable:
                NSLog(@"AFNetworkReachabilityStatusNotReachable");
                break;
            case AFNetworkReachabilityStatusReachableViaWiFi:
                NSLog(@"AFNetworkReachabilityStatusReachableViaWiFi");
                break;
            case AFNetworkReachabilityStatusReachableViaWWAN:
                NSLog(@"AFNetworkReachabilityStatusReachableViaWWAN");
                break;
            default:
                NSLog(@"Unkown network status");
                
        }
        [[ActivityIndicatorClass sharedActivityIndicator] stopAnimating];
        if ([[AFNetworkReachabilityManager sharedManager]isReachable]) {
            [self getUserInfo];
            
            
        }else{
            [self ShowRestAlertWithTitle:@"Lo Sentimos" message:@"No tienes una coneccion a internet."];
        }
        
        
    }];
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
}

-(void)getUserInfo{
    
    [[ActivityIndicatorClass sharedActivityIndicator]startAnimating];
    
    [FACHttpClientUsage getUsersWithEmail: userNameTextField.text AndBlock:^(UserDataObjectClass *  userObject, errorObjectClass *  error) {
        if (!error) {
            self->userContent=userObject;
            
            if ([self->userContent.phone isEqualToString:self->userPassTextField.text]) {
//                ProfileViewController *add = [[ProfileViewController alloc] init];
                [[NSUserDefaults standardUserDefaults] setObject:self->userNameTextField.text forKey:@"email"];

                [self presentViewController:[Util getMainUITabBarController] animated:YES completion:nil];
            }else{
                [self ShowRestAlertWithTitle:@"Datos incorrectos" message:@"se ha creado con exito"];

            }

            [[ActivityIndicatorClass sharedActivityIndicator]stopAnimating];
            
        }else{
            [[ActivityIndicatorClass sharedActivityIndicator]stopAnimating];
            
            [self ShowRestAlertWithTitle:error.title message:error.message];
            
        }
    }];
}


@end
