//
//  ViewController.h
//  ejercicio
//
//  Created by Mac-01 on 28/11/18.
//  Copyright © 2018 JuanSoto. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ViewController : UIViewController
- (IBAction)botonAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *botonNuevo;
@property (weak, nonatomic) IBOutlet UIImageView *profilePic;

@end

NS_ASSUME_NONNULL_END
