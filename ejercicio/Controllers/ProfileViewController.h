//
//  ProfileViewController.h
//  ejercicio
//
//  Created by Mac-01 on 24/11/18.
//  Copyright © 2018 JuanSoto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActivityIndicatorClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface ProfileViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@end

NS_ASSUME_NONNULL_END
