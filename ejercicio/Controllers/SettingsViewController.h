//
//  SettingsViewController.h
//  ejercicio
//
//  Created by Mac-01 on 26/11/18.
//  Copyright © 2018 JuanSoto. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SettingsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@end

NS_ASSUME_NONNULL_END
