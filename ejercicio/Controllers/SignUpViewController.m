//
//  SignUpViewController.m
//  ejercicio
//
//  Created by Mac-01 on 21/11/18.
//  Copyright © 2018 JuanSoto. All rights reserved.
//

#import "SignUpViewController.h"
#import "GlobalInstances.h"
#import "FACHttpClientUsage.h"
#import "AFNetworkReachabilityManager.h"
#import "Util.h"


@interface SignUpViewController (){
    
    UITextField *userNameTextField;
    UITextField *userEmailTextField;
    UITextField *userPassTextField;
    UITextField *userPhoneTextField;
    UITextField *userWebsiteTextField;
    UITextField *userAddressTextField;
    UIButton *maleCheck;
    UIButton *femaleCheck;
    BOOL isFemaleActive;
    BOOL ismaleActive;
    BOOL keyboardIsActive;
    ActivityIndicatorBohra *activityIndicator;
    ActivityIndicatorClass *activityIndicatorClass;
   UserDataObjectClass *usersContent;
    NSString *genderStr;
    
    
}

@end

@implementation SignUpViewController

#pragma mark- Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initialConfigurations];
}

-(void)viewDidAppear:(BOOL)animated{
    [self uiManagement];
//    [self createActivityIndicator];
    
}
-(void)viewWillAppear:(BOOL)animated{
    
}
-(void)viewWillDisappear:(BOOL)animated{
    
}

#pragma mark- Initial Configurations
-(void)initialConfigurations{
    [self boolinitialConfigurations];
}

-(void)boolinitialConfigurations{
    keyboardIsActive=NO;
}

-(void)ShowRestAlertWithTitle: (NSString*)title message:(NSString*)message{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              NSLog(@"OK");
                                                              
                                                          }];
    
    
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark- View Management
-(void)uiManagement{
    [self createImagebackground];
    [self createTwoColorsLabelWithTitle1:@"Heart" title2:@"Beat" fontSize:40 coordinateX:self.view.frame.size.width/2 coordinateY:80 lblColor1:FkColorWhite lblColor2:FkColorBlue];
    [self createLabelWithTitle:@"Sign Up" fontSize:20 coordinateX:self.view.frame.size.width/2 coordinateY:180 lblColor:[UIColor redColor]];
    [self createPassUITextField];
    [self createNameUITextField];
    [self createPhoneUITextField];
    [self createAddressUITextField];
    [self createWebsiteUITextField];
    [self createGenderView];
    [self createEmailUITextField];
    [self createSignUpBtn];
    [self createLabelWithTitle:@"Already have an count?" fontSize:15 coordinateX:self.view.frame.size.width/2 coordinateY:self.view.frame.size.height-100 lblColor:[UIColor redColor]];
    [self createSignInBtn];
    
}

-(void)createLabelWithTitle:(NSString *)titlelbl fontSize:(CGFloat)fontSize coordinateX:(CGFloat)coordinateX coordinateY:(CGFloat)coordinateY lblColor:(UIColor*)lblColor {
    //    UILabel *testLabel = [[UILabel alloc] initWithFrame:CGRectMake(91, 15,2, 1)];
    UILabel *testLabel = [[UILabel alloc] init];
    testLabel.frame = CGRectMake(coordinateX, coordinateY, self.view.frame.size.width, 70);
    testLabel.center = CGPointMake(coordinateX, coordinateY);
    testLabel.text = titlelbl;
    testLabel.numberOfLines = 3;
    //                          testLabel.baselineAdjustment = UIBaselineAdjustmentAlignBaselines; // or UIBaselineAdjustmentAlignCenters, or UIBaselineAdjustmentNone
    testLabel.adjustsFontSizeToFitWidth = NO;
    //    fromLabel.adjustsFontSizeToFitWidth = YES;
    //    testLabel.minimumScaleFactor = 10.0f/12.0f;
    testLabel.font=[UIFont fontWithName:@"Optima" size:fontSize];
    testLabel.clipsToBounds = YES;
    testLabel.backgroundColor = [UIColor colorWithRed:110.0f/255.0f green:159.0f/255.0f blue:224.0f/255.0f alpha:0];
    testLabel.textColor = [UIColor redColor];
    testLabel.textAlignment = NSTextAlignmentCenter;
    testLabel.userInteractionEnabled=NO;
    testLabel.textAlignment = NSTextAlignmentCenter;
    
    
    //    classIsActive=YES;
    [self.view addSubview:testLabel];
}

-(void)createTwoColorsLabelWithTitle1:(NSString *)titlelbl title2:(NSString*)titleLbl2 fontSize:(CGFloat)fontSize coordinateX:(CGFloat)coordinateX coordinateY:(CGFloat)coordinateY lblColor1:(UIColor*)lblColor lblColor2:(UIColor*)lblColor1 {
    //    UILabel *testLabel = [[UILabel alloc] initWithFrame:CGRectMake(91, 15,2, 1)];
    
    
    UILabel *testLabel = [[UILabel alloc] init];
    testLabel.frame = CGRectMake(0, 0, self.view.frame.size.width/2, 140);
    testLabel.text = titlelbl;
    testLabel.clipsToBounds = YES;
    testLabel.textColor = lblColor;
    testLabel.userInteractionEnabled=NO;
    testLabel.minimumScaleFactor = 10.0f/12.0f;
    testLabel.font=[UIFont fontWithName:@"Optima" size:fontSize];
    testLabel.autoresizesSubviews=YES;
    testLabel.textAlignment =NSTextAlignmentRight;
    
    UILabel *testLabel1 = [[UILabel alloc] init];
    testLabel1.frame = CGRectMake(self.view.frame.size.width/2, 0, self.view.frame.size.width, 140);
    testLabel1.text = titleLbl2;
    testLabel1.clipsToBounds = YES;
    testLabel1.textColor = lblColor1;
    testLabel1.userInteractionEnabled=NO;
    testLabel1.minimumScaleFactor = 10.0f/12.0f;
    testLabel1.font=[UIFont fontWithName:@"Optima" size:fontSize];
    testLabel1.textAlignment =NSTextAlignmentLeft;
    
    
    //    classIsActive=YES;
    [self.view addSubview:testLabel];
    [self.view addSubview:testLabel1];
}

-(void)createNameUITextField{
    
    userNameTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-60, 50)];
    userNameTextField.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2-180);
    userNameTextField.attributedPlaceholder =[Util getAtributedStringWithText:@"usuario" andColor:[UIColor blackColor]];
    userNameTextField.backgroundColor = [UIColor whiteColor];
    userNameTextField.textColor = [UIColor blackColor];
    userNameTextField.font = [UIFont systemFontOfSize:14.0f];
    userNameTextField.borderStyle = UITextBorderStyleRoundedRect;
    userNameTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    userNameTextField.returnKeyType = UIReturnKeyDone;
    userNameTextField.textAlignment = NSTextAlignmentLeft;
    userNameTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    userNameTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    userNameTextField.delegate=self;
    userNameTextField.alpha=0.5;
    userNameTextField.tag=1;
    userNameTextField.layer.borderColor=[[UIColor whiteColor]CGColor];
    
    [self.view addSubview:userNameTextField];
}

-(void)createGenderView{
    
    UIView *GenderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-60, 50)];
    GenderView.center=CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2-120);
    GenderView.backgroundColor = [UIColor clearColor];
    GenderView.tag = 5;
    GenderView.userInteractionEnabled=YES;
    
    UIImage *uncheckImage = [UIImage imageNamed:@"unchecked.png"];

    UILabel *maleLabel = [[UILabel alloc] init];
    maleLabel.frame = CGRectMake(0, 0, 100, 50);
    maleLabel.center = CGPointMake(GenderView.frame.size.width/2-30, GenderView.frame.size.height/2);
    maleLabel.text = @"M";
    
    UILabel *femaleLabel = [[UILabel alloc] init];
    femaleLabel.frame = CGRectMake(0, 0, 100, 50);
    femaleLabel.center = CGPointMake(GenderView.frame.size.width/2+30, GenderView.frame.size.height/2);
    femaleLabel.text = @"F";
    
    UILabel *genderLabel = [[UILabel alloc] init];
    genderLabel.frame = CGRectMake(10, GenderView.frame.size.height/2, 100, 50);
    genderLabel.center = CGPointMake(55, GenderView.frame.size.height/2);
    genderLabel.text = @"Gender";
    
    maleCheck = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
    maleCheck.center=CGPointMake(GenderView.frame.size.width/2-50, GenderView.frame.size.height/2);
    [maleCheck addTarget:self action:@selector(maleButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    //    [btnExample addTarget:self action:@selector(metodoBtnExampleDown:) forControlEvents:UIControlEventTouchDown];
    maleCheck.layer.cornerRadius=maleCheck.frame.size.width/2;
    maleCheck.backgroundColor = [UIColor clearColor];
    [maleCheck setImage:uncheckImage forState:UIControlStateNormal];
    maleCheck.layer.masksToBounds=YES;
    
    
    femaleCheck = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
    femaleCheck.center=CGPointMake(GenderView.frame.size.width/2+10, GenderView.frame.size.height/2);
    [femaleCheck addTarget:self action:@selector(femaleButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    //    [btnExample addTarget:self action:@selector(metodoBtnExampleDown:) forControlEvents:UIControlEventTouchDown];
    femaleCheck.layer.cornerRadius=maleCheck.frame.size.width/2;
    [femaleCheck setImage:uncheckImage forState:UIControlStateNormal];
    femaleCheck.backgroundColor = [UIColor clearColor];
    femaleCheck.layer.masksToBounds=YES;

    
    [GenderView addSubview:maleCheck];
    [GenderView addSubview:femaleCheck];
    [GenderView addSubview:genderLabel];
    [GenderView addSubview:maleLabel];
    [GenderView addSubview:femaleLabel];

    [self.view addSubview:GenderView];
}

-(void)createEmailUITextField{
    
    userEmailTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-60, 50)];
    userEmailTextField.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2-60);
    userEmailTextField.attributedPlaceholder =[Util getAtributedStringWithText:@"email" andColor:[UIColor blackColor]];
    userEmailTextField.backgroundColor = [UIColor whiteColor];
    userEmailTextField.textColor = [UIColor blackColor];
    userEmailTextField.font = [UIFont systemFontOfSize:14.0f];
    userEmailTextField.borderStyle = UITextBorderStyleRoundedRect;
    userEmailTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    userEmailTextField.returnKeyType = UIReturnKeyDone;
    userEmailTextField.tag=2;
    userEmailTextField.textAlignment = NSTextAlignmentLeft;
    userEmailTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    userEmailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    userEmailTextField.delegate=self;
    userEmailTextField.keyboardType=UIKeyboardTypeEmailAddress;
    userEmailTextField.alpha=0.5;
    userEmailTextField.tag=2;
    userEmailTextField.layer.borderColor=[[UIColor whiteColor]CGColor];
    
    [self.view addSubview:userEmailTextField];
    
}

-(void)createPhoneUITextField{
    
    userPhoneTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-60, 50)];
    userPhoneTextField.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2);
    userPhoneTextField.attributedPlaceholder =[Util getAtributedStringWithText:@"Phone" andColor:[UIColor blackColor]];
    userPhoneTextField.backgroundColor = [UIColor whiteColor];
    userPhoneTextField.textColor = [UIColor blackColor];
    userPhoneTextField.font = [UIFont systemFontOfSize:14.0f];
    userPhoneTextField.borderStyle = UITextBorderStyleRoundedRect;
    userPhoneTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    userPhoneTextField.returnKeyType = UIReturnKeyDone;
    userPhoneTextField.textAlignment = NSTextAlignmentLeft;
    userPhoneTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    userPhoneTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    userPhoneTextField.delegate=self;
    userPhoneTextField.keyboardType=UIKeyboardTypePhonePad;
    userPhoneTextField.alpha=0.5;
    userPhoneTextField.tag = 3;
    userPhoneTextField.layer.borderColor=[[UIColor whiteColor]CGColor];
    
    [self.view addSubview:userPhoneTextField];
    
}

-(void)createAddressUITextField{
    
    userAddressTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-60, 50)];
    userAddressTextField.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2+60);
    userAddressTextField.attributedPlaceholder =[Util getAtributedStringWithText:@"Address" andColor:[UIColor blackColor]];
    userAddressTextField.backgroundColor = [UIColor whiteColor];
    userAddressTextField.textColor = [UIColor blackColor];
    userAddressTextField.font = [UIFont systemFontOfSize:14.0f];
    userAddressTextField.borderStyle = UITextBorderStyleRoundedRect;
    userAddressTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    userAddressTextField.returnKeyType = UIReturnKeyDone;
    userAddressTextField.textAlignment = NSTextAlignmentLeft;
    userAddressTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    userAddressTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    userAddressTextField.delegate=self;
    userAddressTextField.alpha=0.5;
    userAddressTextField.tag=4;
    userAddressTextField.keyboardType=UIKeyboardTypeURL;
    userAddressTextField.layer.borderColor=[[UIColor whiteColor]CGColor];
    
    [self.view addSubview:userAddressTextField];
    
}

-(void)createWebsiteUITextField{
    
    userWebsiteTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-60, 50)];
    userWebsiteTextField.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2+120);
    userWebsiteTextField.attributedPlaceholder =[Util getAtributedStringWithText:@"Website" andColor:[UIColor blackColor]];
    userWebsiteTextField.backgroundColor = [UIColor whiteColor];
    userWebsiteTextField.textColor = [UIColor blackColor];
    userWebsiteTextField.font = [UIFont systemFontOfSize:14.0f];
    userWebsiteTextField.borderStyle = UITextBorderStyleRoundedRect;
    userWebsiteTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    userWebsiteTextField.returnKeyType = UIReturnKeyDone;
    userWebsiteTextField.textAlignment = NSTextAlignmentLeft;
    userWebsiteTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    userWebsiteTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    userWebsiteTextField.delegate=self;
    userWebsiteTextField.alpha=0.5;
    userWebsiteTextField.tag=5;
    userWebsiteTextField.layer.borderColor=[[UIColor whiteColor]CGColor];
    
    [self.view addSubview:userWebsiteTextField];
    
}

-(void)createPassUITextField{
    
    userPassTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-60, 50)];
    userPassTextField.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2+180);
    userPassTextField.attributedPlaceholder =[Util getAtributedStringWithText:@"password" andColor:[UIColor blackColor]];
    userPassTextField.backgroundColor = [UIColor whiteColor];
    userPassTextField.textColor = [UIColor blackColor];
    userPassTextField.font = [UIFont systemFontOfSize:14.0f];
    userPassTextField.borderStyle = UITextBorderStyleRoundedRect;
    userPassTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    userPassTextField.returnKeyType = UIReturnKeyDone;
    userPassTextField.textAlignment = NSTextAlignmentLeft;
    userPassTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    userPassTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    userPassTextField.delegate=self;
    userPassTextField.alpha=0.5;
    userPassTextField.tag=6;
    userPassTextField.layer.borderColor=[[UIColor whiteColor]CGColor];
    
    [self.view addSubview:userPassTextField];
    
}

-(void)createSignUpBtn{
    UIButton *SignUpBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-60, 50)];
    SignUpBtn.center=CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2+240);
    SignUpBtn.backgroundColor=[UIColor redColor];
    [SignUpBtn addTarget:self action:@selector(signUpBtnSelector:) forControlEvents:UIControlEventTouchUpInside];
    //    [btnExample addTarget:self action:@selector(metodoBtnExampleDown:) forControlEvents:UIControlEventTouchDown];
    [SignUpBtn setTitle:@"Sign Up" forState:UIControlStateNormal];
    SignUpBtn.layer.cornerRadius=5;
    
    
    [self.view addSubview:SignUpBtn];
}

-(void)createSignInBtn{
    UIButton *createSignInBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 140, 30)];
    createSignInBtn.center=CGPointMake(self.view.frame.size.width/2+110, self.view.frame.size.height-100);
    createSignInBtn.backgroundColor=[UIColor clearColor];
    [createSignInBtn addTarget:self action:@selector(signInBtnSelector:) forControlEvents:UIControlEventTouchUpInside];
    [createSignInBtn setTitle:@"Sign In" forState:UIControlStateNormal];
    
    [self.view addSubview:createSignInBtn];
}

-(void)createImagebackground{
    UIImageView *backgroundImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    backgroundImage.backgroundColor=[UIColor redColor];
    backgroundImage.layer.cornerRadius=backgroundImage.frame.size.width/2;
    [backgroundImage setImage:[UIImage imageNamed:@"background.jpg"]];
    backgroundImage.userInteractionEnabled=YES;
    
    UITapGestureRecognizer *tapRecognizer= [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hadleSingleTap:)];
    [tapRecognizer setNumberOfTapsRequired:1];
    [backgroundImage addGestureRecognizer:tapRecognizer];
    
    [self.view addSubview:backgroundImage];
}

-(void)hadleSingleTap:(UITapGestureRecognizer*)recognizer{
    [self resignTextFields];
}

-(void)resignTextFields{
    [self moveMainViewDown];
    [userNameTextField resignFirstResponder];
    [userEmailTextField resignFirstResponder];
    [userPhoneTextField resignFirstResponder];
    [userAddressTextField resignFirstResponder];
    [userWebsiteTextField resignFirstResponder];
    [userPassTextField resignFirstResponder];
}


#pragma mark- UIButtonMethods


-(void)femaleButtonAction:(UIButton*)btn{
    

    UIImage *checkImage = [UIImage imageNamed:@"checked.png"];
    UIImage *uncheckImage = [UIImage imageNamed:@"unchecked.png"];
    
    isFemaleActive = YES;
    genderStr=@"female";
    [femaleCheck setImage:checkImage forState:UIControlStateNormal];
    if (isFemaleActive) {
        ismaleActive=NO;
        [maleCheck setImage:uncheckImage forState:UIControlStateNormal];
    }
    
}

-(void)maleButtonAction:(UIButton*)btn{
    
    UIImage *checkImage = [UIImage imageNamed:@"checked.png"];
    UIImage *uncheckImage = [UIImage imageNamed:@"unchecked.png"];
    genderStr=@"male";

    ismaleActive = YES;
    [maleCheck setImage:checkImage forState:UIControlStateNormal];
    if (isFemaleActive) {
        isFemaleActive=NO;
        [femaleCheck setImage:uncheckImage forState:UIControlStateNormal];
    }

}


-(void)signUpBtnSelector:(UIButton*)btn{
    if ([self isCompletedForm]) {
        [self initialAPIRequest];
    }
    
}

-(void)signInBtnSelector:(UIButton*)btn{
    [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];

    
}

-(void)createActivityIndicator{
    activityIndicator=[[ActivityIndicatorBohra alloc]initWithFrame:self.view.frame];
    [self.view addSubview:activityIndicator];
    [activityIndicator show];
    
}



#pragma mark- API Request
-(void)initialAPIRequest{
    
    [[ActivityIndicatorClass sharedActivityIndicator] startAnimating];
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        
        switch (status) {
            case AFNetworkReachabilityStatusNotReachable:
                NSLog(@"AFNetworkReachabilityStatusNotReachable");
                break;
            case AFNetworkReachabilityStatusReachableViaWiFi:
                NSLog(@"AFNetworkReachabilityStatusReachableViaWiFi");
                break;
            case AFNetworkReachabilityStatusReachableViaWWAN:
                NSLog(@"AFNetworkReachabilityStatusReachableViaWWAN");
                break;
            default:
                NSLog(@"Unkown network status");
                
        }
        [[ActivityIndicatorClass sharedActivityIndicator] stopAnimating];
        if ([[AFNetworkReachabilityManager sharedManager]isReachable]) {
            [self createUsersWithUserName:self->userNameTextField.text userEmail:self->userEmailTextField.text identifier:self->userPassTextField.text phone:self->userPhoneTextField.text address:self->userAddressTextField.text website:self->userWebsiteTextField.text gender:self->genderStr];

            
        }else{
            [self ShowRestAlertWithTitle:@"Lo Sentimos" message:@"No tienes una coneccion a internet."];
        }
        
        
    }];
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
}


-(void)createUsersWithUserName: (NSString*)userName userEmail: (NSString*)userMail identifier: (NSString*)identidentifier phone: (NSString*)phone address: (NSString*)address website: (NSString*)website gender: (NSString*)gender{
    [[ActivityIndicatorClass sharedActivityIndicator]startAnimating];

    [FACHttpClientUsage createUsersWithUserName:userName userEmail:userMail
                                     identifier:identidentifier phone: phone address: address website: website gender: gender AndBlock:^(NSDictionary * _Nonnull usersDic, errorObjectClass * _Nonnull error) {
                                         if (!error) {
                                             [[ActivityIndicatorClass sharedActivityIndicator]stopAnimating];
                                             
                                             [[NSUserDefaults standardUserDefaults] setObject:self->userEmailTextField.text forKey:@"email"];
                                             [[NSNotificationCenter defaultCenter]postNotificationName:@"SignUpEvent" object:self];
                                             [self presentViewController:[Util getMainUITabBarController] animated:YES completion:nil];

                                         }else{
                                             [[ActivityIndicatorClass sharedActivityIndicator]stopAnimating];
                                             [self ShowRestAlertWithTitle:error.title message:error.message];

                                         }
                                     }];
    
}
#pragma mark- Others
-(BOOL)isCompletedForm{
    
    BOOL isCompletedForm=YES;
if ([userNameTextField.text isEqualToString:@""]) {
    [self ShowRestAlertWithTitle:@"Error" message:@"debe completar el nombre de usuario"];
    isCompletedForm=NO;
}
if (genderStr == nil) {
    [self ShowRestAlertWithTitle:@"Error" message:@"debe seleccionar un genero"];
    isCompletedForm=NO;
}else if ([userEmailTextField.text isEqualToString:@""]) {
    [self ShowRestAlertWithTitle:@"Error" message:@"debe completar el email"];
    isCompletedForm=NO;
}else if (![Util isValidEmail:userEmailTextField.text]){
    [self ShowRestAlertWithTitle:@"Error" message:@"email invalido"];
    isCompletedForm=NO;
}else if ([userPhoneTextField.text isEqualToString:@""]) {
    [self ShowRestAlertWithTitle:@"Error" message:@"debe completar el numero de celular"];
    isCompletedForm=NO;
}else if ([userAddressTextField.text isEqualToString:@""]) {
    [self ShowRestAlertWithTitle:@"Error" message:@"debe completar la direccion"];
    isCompletedForm=NO;
}else if ([userWebsiteTextField.text isEqualToString:@""]) {
    [self ShowRestAlertWithTitle:@"Error" message:@"debe completar el website"];
    isCompletedForm=NO;
}else if ([userPassTextField.text isEqualToString:@""]) {
    [self ShowRestAlertWithTitle:@"Error" message:@"debe completar el password"];
    isCompletedForm=NO;
}
    return isCompletedForm;
}

#pragma mark- Animations

-(void)mainView:(UITextField*)textField{
    if (textField == userAddressTextField || textField == userWebsiteTextField || textField == userPassTextField )  {
        if (!keyboardIsActive) {
            [self moveMainViewUp];
        }
        }else{
            [self moveMainViewDown];
    }
}
-(void)moveMainViewUp{
    keyboardIsActive = YES;
    [UIView animateWithDuration:0.3 animations:^{
        self.view.center=CGPointMake(self.view.center.x, self.view.frame.size.height*0.3);
    }];
    
}

-(void)moveMainViewDown{
    keyboardIsActive = NO;
        [UIView animateWithDuration:0.3 animations:^{
            self.view.center=CGPointMake(self.view.center.x, self.view.frame.size.height/2);
        }];
    
}


#pragma mark- UITextFieldDelegate

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    textField.backgroundColor = [UIColor colorWithRed:220.0f/255.0f green:220.0f/255.0f blue:220.0f/255.0f alpha:1.0f];
    textField.attributedPlaceholder =[Util getAtributedStringWithText:textField.placeholder andColor:[UIColor grayColor]];
    [self mainView:textField];
    
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    NSLog(@"textFieldShouldEndEditing");
    textField.backgroundColor = [UIColor whiteColor];
    textField.attributedPlaceholder =[Util getAtributedStringWithText:textField.placeholder andColor:[UIColor blackColor]];
    
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    NSLog(@"textFieldDidEndEditing");
}


- (BOOL)textFieldShouldClear:(UITextField *)textField{
    NSLog(@"textFieldShouldClear:");
    return YES;
}

//Pasar por textfield con el boton aceptar
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    NSLog(@"textFieldShouldReturn:");
    NSInteger nextTag = textField.tag + 1;
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder){
        [nextResponder becomeFirstResponder];
    } else{
        [textField resignFirstResponder];
    }
    
    return YES;
}


@end

