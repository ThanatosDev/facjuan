//
//  SignUpViewController.h
//  ejercicio
//
//  Created by Mac-01 on 21/11/18.
//  Copyright © 2018 JuanSoto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "errorObjectClass.h"
#import "ActivityIndicatorBohra.h"
#import "ActivityIndicatorClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface SignUpViewController : UIViewController<UITextFieldDelegate>

@end

NS_ASSUME_NONNULL_END
