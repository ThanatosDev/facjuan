//
//  SettingsViewController.m
//  ejercicio
//
//  Created by Mac-01 on 26/11/18.
//  Copyright © 2018 JuanSoto. All rights reserved.
//

#import "SettingsViewController.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

#pragma mark- Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self uiManagement];
    
    
}

-(void)viewDidAppear:(BOOL)animated{
    
}
-(void)viewWillAppear:(BOOL)animated{
    
}
-(void)viewWillDisappear:(BOOL)animated{
    
}

#pragma mark- View Management
-(void)uiManagement{
    [self createUITableView];
    
    
}

-(void)createUITableView {
    UITableView *profileTableView = [[UITableView alloc] init];
    profileTableView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    profileTableView.delegate = self;
    profileTableView.dataSource = self;
    
    [self.view addSubview:profileTableView];
}


#pragma mark- UITableViewDelegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)theTableView
{
    return 2;
}

// number of row in the section, I assume there is only 1 row
- (NSInteger)tableView:(UITableView *)theTableView numberOfRowsInSection:(NSInteger)section
{
    
    NSInteger numberOfRows = 0;
    if (section==0) {
        numberOfRows=3;
    }else{
        numberOfRows=20;
    }
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"newFriendCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UIImageView *ProfilePicture=[[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 30, 30)];
    ProfilePicture.backgroundColor=[UIColor redColor];
    ProfilePicture.layer.cornerRadius=ProfilePicture.frame.size.width/2;
    [ProfilePicture setImage:[UIImage imageNamed:@"settings.png"]];
    ProfilePicture.layer.masksToBounds=YES;
    
    
    UILabel *ProfileNameLabel = [[UILabel alloc] init];
    ProfileNameLabel.frame = CGRectMake(50, 10, 100, 20);
    ProfileNameLabel.text = @"sadasdas";
    ProfileNameLabel.adjustsFontSizeToFitWidth = YES;
    ProfileNameLabel.textColor = [UIColor blackColor];
    ProfileNameLabel.textAlignment = NSTextAlignmentLeft;
    ProfileNameLabel.userInteractionEnabled=NO;
    
    [cell addSubview:ProfilePicture];
    [cell addSubview:ProfileNameLabel];

    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 50;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}
//HeaderFor
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    headerView.backgroundColor = [UIColor grayColor];
    headerView.userInteractionEnabled=YES;
    headerView.alpha=1;
    
    if (section==0) {
        
        
        UILabel *headerLabel=[[UILabel alloc]initWithFrame:CGRectMake(10, 10, 250, 22)];
        headerLabel.backgroundColor=[UIColor clearColor];
        headerLabel.center = CGPointMake(150, headerView.frame.size.height/2);
        headerLabel.font=[UIFont fontWithName:@"Arial" size:16];
        headerLabel.text=@"Accesos directos";
        headerLabel.textColor=[UIColor whiteColor];
        
        UIButton *editBtn = [[UIButton alloc]initWithFrame:CGRectMake(headerView.frame.size.width-100, headerView.frame.size.height/2-10, 80, 20)];
        editBtn.center = CGPointMake(headerView.frame.size.width-50, headerView.frame.size.height/2);
        editBtn.backgroundColor=[UIColor redColor];
        [editBtn addTarget:self action:@selector(LoginBtnSelector:) forControlEvents:UIControlEventTouchUpInside];
        [editBtn setTitle:@"Editar" forState:UIControlStateNormal];
        
        
        [headerView addSubview:editBtn];
        
        [headerView addSubview:headerLabel];
    }else{
        
        UILabel *headerLabel=[[UILabel alloc]initWithFrame:CGRectMake(10, 10, 250, 22)];
        headerLabel.center = CGPointMake(150, headerView.frame.size.height/2);
        headerLabel.backgroundColor=[UIColor clearColor];
        headerLabel.font=[UIFont fontWithName:@"Arial" size:16];
        headerLabel.text=@"Explorar";
        headerLabel.textColor=[UIColor whiteColor];
        
        [headerView addSubview:headerLabel];
    }
    
    return headerView;
    
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return true;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
}





@end
