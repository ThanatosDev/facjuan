//
//  GlobalInstances.h
//  ejercicio
//
//  Created by Mac-01 on 21/11/18.
//  Copyright © 2018 JuanSoto. All rights reserved.
//

#ifndef GlobalInstances_h
#define GlobalInstances_h


//Colors
#define FkColorRed [UIColor redColor]
#define FkColorBlue [UIColor blueColor]
#define FkColorWhite [UIColor whiteColor]


#define FkApiRestBaseUrl @"https://gorest.co.in"
#define FkApiVersion_Public @"public-api"
#define FkApiTokenString @"ggolvSv4UpUH_a9Qk5x5KAC2YudbptpltVYZ"
#define FkApiTokenKey @"tokenKey"


#define FkRestUserDataLinksSelfHref @"href"
#define FkRestUserDataLinksAvatarHref @"href"
#define FkRestUserDataId @"id"
#define FkRestUserDataName @"name"
#define FkRestUserDataGender @"gender"
#define FkRestUserDataDob @"dob"
#define FkRestUserDataPhone @"phone"
#define FkRestUserDataWebsite @"website"
#define FkRestUserDataAddress @"address"
#define FkRestUserDataStatus @"status"
#define FkRestUserDataEmail @"email"
#define FkRestUserDataLinks @"_links"
#define FkRestUserDataLinksSelf @"self"
#define FkRestUserDataLinksAvatar @"avatar"
#define FkRestUserDataLinksSelfHref @"href"
#define FkRestUserDataPost @"posts"
#define FkRestUserDataPostid @"id"
#define FkRestUserDataPostUserId @"user_id"
#define FkRestUserDataPostTittle @"title"
#define FkRestUserDataPostBody @"body"



#define FkRestUserName @"juan soto"

#endif /* GlobalInstances_h */
